const { app, BrowserWindow } = require('electron')

function createWindow(){
	let win = new BrowserWindow({
		width:300,
		height:220,
		x:100,
		y:200,
		frame: false,
		resizable: false,	// 窗口大小不可调
		webPreferences:{
			nodeIntegration: true
		}
	})
	win.loadFile('./index.html')
}

app.whenReady().then(()=>createWindow())